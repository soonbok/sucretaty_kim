// ChildDataDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "afxdialogex.h"
#include "sucretary_kim.h"
#include "ChildDataDlg.h"


// CChildDataDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CChildDataDlg, CDialogEx)

CChildDataDlg::CChildDataDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CChildDataDlg::IDD, pParent)
{
}

CChildDataDlg::~CChildDataDlg()
{
}

void CChildDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CChildDataDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CChildDataDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CChildDataDlg 메시지 처리기입니다.

BOOL CChildDataDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	// Edit box 포커스 설정
	GetDlgItem(IDC_EDIT_NAME)->SetFocus();

	// 생성
	CComboBox* pCombo = (CComboBox*)GetDlgItem(IDC_COMBO_COMMEND);
	pCombo->ResetContent();

	// 아이템 추가
	pCombo->AddString(_T("실행"));
//	pCombo->AddString(_T("검색"));

	// 아이템 선택
	pCombo->SetCurSel(0);

	// 해제
	pCombo->Clear();

	return FALSE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CChildDataDlg::OnBnClickedOk()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	// 생성
	CComboBox* pCombo = (CComboBox*)GetDlgItem(IDC_COMBO_COMMEND);

	// 선택된 아이템 얻기
	int nSelect = pCombo->GetCurSel();
	pCombo->GetLBText(nSelect, m_strChildCommend);

	GetDlgItemText(IDC_EDIT_NAME, m_strChildName);
	GetDlgItemText(IDC_EDIT_PATH, m_strChildPath);

	// 해제
	pCombo->Clear();

	CDialogEx::OnOK();
}

CString CChildDataDlg::GetChildName()
{
	return m_strChildName;
}
CString CChildDataDlg::GetChildPath()
{
	return m_strChildPath;
}
CString CChildDataDlg::GetChildCommend()
{
	return m_strChildCommend;
}



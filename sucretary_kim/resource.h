//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by sucretary_kim.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SUCRETARY_KIM_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_ADD                  129
#define IDR_MENU_TRAY                   131
#define IDI_ICON_TRAY                   132
#define IDR_ACCELERATOR1                133
#define IDC_BUTTON_ADD                  1000
#define IDC_EDIT_INPUT                  1001
#define IDC_EDIT_NAME                   1002
#define IDC_EDIT_PATH                   1003
#define IDC_COMBO_COMMEND               1005
#define IDC_CHECK1                      1006
#define IDC_CHECK_CHROME                1006
#define ID_FILES_32771                  32771
#define ID_FILES_32772                  32772
#define ID_FILES_32773                  32773
#define ID_FILES_RUN                    32774
#define ID_FILES_ADD                    32775
#define ID_FILES_EXIT                   32776
#define ID_ACCELERATOR_RUN              32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

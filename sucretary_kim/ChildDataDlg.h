#pragma once


// CChildDataDlg 대화 상자입니다.

class CChildDataDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CChildDataDlg)

public:
	CChildDataDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CChildDataDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_ADD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();

public:
	CString m_strChildName;
	CString m_strChildPath;
	CString m_strChildCommend;

public:
	CString GetChildName();
	CString GetChildPath();
	CString GetChildCommend();
	virtual BOOL OnInitDialog();
};


// stdafx.cpp : 표준 포함 파일만 들어 있는 소스 파일입니다.
// sucretary_kim.pch는 미리 컴파일된 헤더가 됩니다.
// stdafx.obj에는 미리 컴파일된 형식 정보가 포함됩니다.

#include "stdafx.h"

int writeFile(CString strFileName, CString strData)
{
	// 생성
	ofstream file;
	file.open(strFileName, ios_base::out | ios_base::app);
	
	// 파일 쓰기
	file << strData <<endl;

	// 해제
	file.close();

	return 0;
}

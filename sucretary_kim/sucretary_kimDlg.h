
// sucretary_kimDlg.h : 헤더 파일
//

#pragma once
#include "afxwin.h"


struct NOTIFYICONDATAEX
{
	DWORD cbSize;          // Size of this structure, in bytes.
	HWND hWnd;             // Handle to the window that will receive notification messages associated with an icon in the taskbar status area. The shell uses hWnd and uID to identify which icon is to be operated on when Shell_NotifyIcon is invoked.
	UINT uID;              // Application-defined identifier of the taskbar icon. The shell uses hWnd and uID to identify which icon is to be operated on when Shell_NotifyIcon is invoked. You can have multiple icons associated with a single hWnd by assigning each a different uID.
	UINT uFlags;           // Array of flags that indicate which of the other members contain valid data
	UINT uCallbackMessage; // Application-defined message identifier
	HICON hIcon;           // Handle to the icon to be added, modified, or deleted.
	TCHAR szTip[128];      // Pointer to a NULL-terminated string with the text for a standard tooltip.
	DWORD dwState;         // State of the icon
	DWORD dwStateMask;     // A value that specifies which bits of the state member will be retrieved or modified.
	TCHAR szInfo[256];     // Pointer to a NULL-terminated string with the text for a balloon-style tooltip
	union {
		UINT  uTimeout;    // The timeout value, in milliseconds, for a balloon-style tooltip.
		UINT  uVersion;    // Used to specify whether the shell notify icon interface should use Windows 95 or Windows 2000 behavior.
	} DUMMYUNIONNAME;
	TCHAR szInfoTitle[64]; // Pointer to a NULL-terminated string containing a title for a balloon tooltip.
	DWORD dwInfoFlags;     // Flags that can be set to add an icon to a balloon tooltip
};

// Csucretary_kimDlg 대화 상자
class Csucretary_kimDlg : public CDialogEx
{
// 생성입니다.
public:
	Csucretary_kimDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	~Csucretary_kimDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SUCRETARY_KIM_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonAdd();

//
///////////////////////////////////////////////////////////////////////////
// 사용자 정의 Function
public:
	struct t_ConfData 
	{
		CString strName;
		CString strPath;
		CString strCommend;
	};

	WCArray<struct t_ConfData> m_tConfData;

public:
	int writeConfFile(CString strName, CString strPath, CString strCommend);
	int readFile(CString strFileName);
	int commendRun(CString strInput);
	int executeProcess(struct t_ConfData tConfData);
	int webBrowserSearch(CString strInput);

public:
	CString GetPortalName(CString strInput);
	CString GetSearchString(CString strInput);
	afx_msg void OnEnSetfocusEditInput();
	LRESULT OnTrayMessage(WPARAM wParam,LPARAM lParam);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg LRESULT OnHotKey(WPARAM wParam, LPARAM lParam);


private:
	void CreateTrayIcon();

	CButton m_chkChrome;
	NOTIFYICONDATAEX m_nid;
	INT m_nHotKeyID;
	INT m_nHotKeyID_FileDelete;
	BOOL m_bInitDlg;
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};

/*****************************************************************************
 *                                                                           *
 * File Name : swc_array.h                                                   *
 * Theme     : std:vector wrapper class (array)                              *
 * Author    : Park, sung soo                                                *
 * History   : 1. created by  Park, sung soo '09. 10. 26                     *
 *                                                                           *
 *****************************************************************************/

#ifdef __LINK_COMMON__
	#undef min
	#undef max
#endif

#include <iostream>
#include <vector>

#ifdef __SWC_OS_LINUX__
	#include <pthread.h>
#endif 

//#include <swc_base.h>

#ifndef __SWC_ARRAY_H__
#define __SWC_ARRAY_H__

//-----------------------------------------------------------------------------
// GLOBAL DEFINITIONS
//-----------------------------------------------------------------------------

#define ARRAY_ITERATOR  typename std::vector<TYPE>::iterator
#define ARRAY_RITERATOR typename std::vector<TYPE>::reverse_iterator

/// array template class (std::vector wrapper)
template < typename TYPE, 
           typename ALLOCATOR = std::allocator < TYPE > >
class WCArray
{
public :
	UINT Add(TYPE rData)
	{
		m_Array.push_back(rData);
		return (UINT)m_Array.size();
	}
	UINT Append(WCArray& src)
	{
		ARRAY_ITERATOR it_1_s, it_1_l, it_2_s, it_2_l;
		std::vector<TYPE> _tarray;

		_tarray=this->m_Array;	

		it_1_s = _tarray.begin(); 
		it_1_l = it_1_s + _tarray.size();
		it_2_s = src.m_Array.begin();
		it_2_l = it_2_s + src.m_Array.size();
		this->m_Array.resize(_tarray.size() + src.m_Array.size());

		merge(it_1_s, it_1_l, it_2_s, it_2_l, this->m_Array.begin()); 
		_tarray.clear();
		return (UINT)m_Array.size();
	}
	TYPE& ElementAt(UINT nIndex)
	{
		return m_Array.at(nIndex);
	}
	const TYPE& ElementAt(UINT nIndex) const
	{
		return m_Array.at(nIndex);
	}
	TYPE& GetAt(UINT nIndex)
	{
		return m_Array.at(nIndex);
	}
	const TYPE& GetAt(UINT nIndex) const
	{
		return m_Array.at(nIndex);
	}
	UINT GetCount(VOID) const
	{
		UINT lret = (UINT)m_Array.size();
		if(1000000 < lret) lret = 0;
		return lret;
	}
	UINT GetSize(VOID) const
	{
		return (UINT)m_Array.capacity();
	}
	INT GetUpperBound(VOID) const
	{
		if(0 == m_Array.size()) return -1;
		return (INT)(m_Array.size() -1);
	}
	bool InsertAt(UINT nIndex, TYPE newData)
	{
		bool       bret = FALSE;
		UINT           ncur = 0;
		ARRAY_ITERATOR curit;

		for(curit = m_Array.begin(); curit != m_Array.end(); curit++)
		{
			if(ncur == nIndex)
			{
				m_Array.insert(curit, newData);
				bret = TRUE;
				break;
			}
			ncur++;
		}
		return bret;
	}
	bool InsertAt(UINT nIndex, WCArray* pNewArray)
	{
		bool       bret = FALSE;
		UINT           ncur = 0;
		ARRAY_ITERATOR curit;
		TYPE           curDatum;

		if(NULL == pNewArray) return FALSE;
		curDatum = pNewArray->GetAt(nIndex);
		for(curit = m_Array.begin(); curit != m_Array.end(); curit++)
		{
			if(ncur == nIndex)
			{
				m_Array.insert(curit, curDatum);
				bret = TRUE;
				break;
			}
			ncur++;
		}
		return bret;
	}
	bool IsEmpty(VOID)
	{
		return m_Array.empty();
	}
	VOID RemoveAll(VOID)
	{
		//TYPE *p;

		m_Array.clear();
		//m_Array.get_allocator().deallocate(p, m_Array.size());
	}
	bool RemoveAt(UINT nIndex)
	{
		bool       bret = FALSE;
		UINT           ncur = 0;
		ARRAY_ITERATOR curit;
		
		for(curit = m_Array.begin(); curit != m_Array.end(); curit++)
		{
			if(ncur == nIndex)
			{
				m_Array.erase(curit);
				bret = TRUE;
				break;
			}
			ncur++;
		}
		return bret;
	}
	bool SetAt(UINT nIndex, TYPE newData)
	{
		if(nIndex >= m_Array.size()) return FALSE;
		m_Array.at(nIndex) = newData;
		return TRUE;
	}
	VOID SetAtGrow(UINT nIndex, TYPE newData)
	{
		if(nIndex >= m_Array.size())
			m_Array.push_back(newData);
		else
			m_Array.at(nIndex) = newData;
	}
	VOID SetSize(UINT nNewSize)
	{
		m_Array.reserve(nNewSize);
	}
	VOID SetResize(UINT nNewSize)
	{
		m_Array.resize(nNewSize);
	}
	TYPE& operator [] (UINT nIndex)
	{
		return m_Array.at(nIndex);
	}
	const TYPE& operator [] (UINT nIndex) const
	{
		return m_Array.at(nIndex);
	}

public :
	std::vector<TYPE> m_Array;

private :
	ARRAY_ITERATOR    m_Iterator;
	ARRAY_RITERATOR   m_rIterator;

};

//-----------------------------------------------------------------------------
#endif // __SWC_ARRAY_H__
 

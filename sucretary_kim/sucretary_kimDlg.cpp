
// sucretary_kimDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "sucretary_kim.h"
#include "sucretary_kimDlg.h"
#include "afxdialogex.h"
#include "ChildDataDlg.h"

// Default 입력 언어 한글
#include <imm.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_TRAY_EVENT	WM_USER + 500	// 트래이 이벤트 처리 위한 정의
#define TIMER_KEY_DEBUG_START 1000

// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.


class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Csucretary_kimDlg 대화 상자




Csucretary_kimDlg::Csucretary_kimDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(Csucretary_kimDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON_TRAY);
	ZeroMemory(&m_nid, sizeof(m_nid));

	m_nHotKeyID = 0;
	m_nHotKeyID_FileDelete = 0;
	m_bInitDlg = TRUE;
}

Csucretary_kimDlg::~Csucretary_kimDlg()
{
	m_tConfData.RemoveAll();

	// 작업 표시줄(TaskBar)의 상태 영역에 아이콘을 삭제한다.
	Shell_NotifyIcon(NIM_DELETE, (NOTIFYICONDATA*)&m_nid);
}

void Csucretary_kimDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_CHROME, m_chkChrome);
}

BEGIN_MESSAGE_MAP(Csucretary_kimDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &Csucretary_kimDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_ADD, &Csucretary_kimDlg::OnBnClickedButtonAdd)
	ON_EN_SETFOCUS(IDC_EDIT_INPUT, &Csucretary_kimDlg::OnEnSetfocusEditInput)
	ON_MESSAGE(WM_TRAY_EVENT, OnTrayMessage)
	ON_MESSAGE(WM_HOTKEY, OnHotKey)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// Csucretary_kimDlg 메시지 처리기

BOOL Csucretary_kimDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	CreateTrayIcon();

	// 단축키 로드
	m_nHotKeyID = ::GlobalAddAtom("HotKey_SucretaryKim");
	::RegisterHotKey(GetSafeHwnd(), m_nHotKeyID, MOD_CONTROL|MOD_SHIFT, 0x20);

	m_nHotKeyID_FileDelete = ::GlobalAddAtom("HotKey_FileDelete");
	::RegisterHotKey(GetSafeHwnd(), m_nHotKeyID_FileDelete, MOD_SHIFT, 0x2E);

	GetDlgItem(IDC_EDIT_INPUT)->SetFocus();
	CheckDlgButton(IDC_CHECK_CHROME, TRUE);


	return FALSE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void Csucretary_kimDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void Csucretary_kimDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR Csucretary_kimDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//***********************************************************/
// 사용자 정의 함수
//***********************************************************/
#define _MSG AfxMessageBox
//
/////////////////////////////////////////////////////////////
// 명령 실행
void Csucretary_kimDlg::OnBnClickedOk()
{	
	// 입력 문자열 읽기
	CString strEditString;
	GetDlgItemText(IDC_EDIT_INPUT, strEditString);	
	CString strLower = strEditString.MakeLower();

	// 파일 오픈 및 설정 파일 읽음.
	readFile(_T("sucretary_kim.txt"));

	// 분류된 항목 파싱 및 실행
	commendRun(strLower.Trim());

	ShowWindow(SW_HIDE);
}

//
/////////////////////////////////////////////////////////////
// 명령 추가
void Csucretary_kimDlg::OnBnClickedButtonAdd()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CChildDataDlg addCommendDlg;
	CString strName, strPath, strCommend;

	switch (addCommendDlg.DoModal())
	{
	case -1:
		_MSG(_T("Dialog box could not be created!"));
		break;

	case IDOK:	
		// 자식 Dialog 객체 생성		
		strName.Format(_T("%s"), addCommendDlg.GetChildName());
		strPath.Format(_T("%s"), addCommendDlg.GetChildPath());
		strCommend.Format(_T("%s"), addCommendDlg.GetChildCommend());

		// Conf 파일 쓰기
		writeConfFile(strName, strPath, strCommend);
		break;

	default :
		break;
	}
}

int Csucretary_kimDlg::writeConfFile(CString strName, CString strPath, CString strCommend)
{	
	CString strBuffer;
	strBuffer.Format(_T("%s^%s^%s^"), strName, strPath, strCommend);

	// 파일 쓰기
	writeFile(_T("sucretary_kim.txt"), strBuffer);

	return 0;
}

int Csucretary_kimDlg::readFile(CString strFileName)
{
	// 생성
	ifstream file;
	file.open(strFileName, ios_base::in);
	
	// 파일 읽기
	char* pOutData = new char[5000];

	int nFlag = 0;
	struct t_ConfData tconf;

	while (!file.eof()) {

		file.getline(pOutData, 5000, '^');
	
		// 마지막 ^가 존재 하므로 마지막이 줄바꿈이면 break 함
		if (0 == _stricmp(pOutData, "\n")) {
			break;
		}
		
		nFlag++;
		switch (nFlag) {

		case 1:
			tconf.strName.Format(_T("%s"), _strlwr(pOutData));			
			break;
		case 2:
			tconf.strPath.Format(_T("%s"), pOutData);
			break;
		case 3:
			tconf.strCommend.Format(_T("%s"), pOutData);
			
			// 벡터 추가
         	m_tConfData.Add(tconf);

			// flag 초기화
			nFlag = 0;
			break;
		default:
			break;
		}
	}

	// 해제
	delete[] pOutData;
	file.close();

	return 0;
}

int Csucretary_kimDlg::commendRun(CString strInput)
{
	struct t_ConfData tconf;

	for (UINT i=0; i<m_tConfData.GetCount(); i++) {	

		tconf = m_tConfData[i];

		// 실행 시킴
		if (0 <= strInput.Find("실행"))	{

			// 이름이 존재하는가?
			if (0 > strInput.Find(tconf.strName.Trim(), 0)) {
				continue;
			}

			// 명령이 존재하는가?
			if (0 > strInput.Find(tconf.strCommend.Trim(), 0)) {
				continue;
			}

			executeProcess(tconf);
		}
		else if (0 <= strInput.Find("검색")) {
			webBrowserSearch(strInput);
		}
		else if (0 <= strInput.Find(".")) {
			webBrowserSearch(strInput);
		}
		// 실행도 검색도 아니다
		else {
			// 이름이 동일한가?
			if (0 != strInput.CompareNoCase(tconf.strName.Trim())) {
				continue;
			}

			// 경로가 존재하는가?
			if (TRUE == tconf.strPath.IsEmpty()) {
				continue;
			}

			executeProcess(tconf);
		}

		break;
	}

	return 0;
}

int Csucretary_kimDlg::executeProcess(struct t_ConfData tConfData)
{
	//_MSG(tconf.strName);
	CString strParam;
	if (0 == tConfData.strName.CompareNoCase("피클삭제")) {
		strParam.Format("[4m@n0901]");
	}
	else if (0 == tConfData.strName.CompareNoCase("디버깅시작")) {
		strParam.Format("-protectionoff puca");
		SetTimer(TIMER_KEY_DEBUG_START, 60000, NULL);
	}
	else if (0 == tConfData.strName.CompareNoCase("디버깅종료")) {
		KillTimer(TIMER_KEY_DEBUG_START);
		return 0;
	}

	ShellExecute(NULL, NULL, tConfData.strPath, strParam, NULL, SW_SHOW);

	return 0;
}

int Csucretary_kimDlg::webBrowserSearch(CString strInput)
{
	//ShellExecute(this->m_hWnd, _T("open"), _T("IEXPLORE.EXE"), NULL, NULL, SW_SHOW);

	CString strPortal = GetPortalName(strInput);
	CString strSearch = GetSearchString(strInput);
	CString strFull;
	
	strFull.Format("%s\"%s\"", strPortal.Trim(), strSearch.Trim());

	// 크롬 체크 상태
	ShellExecute(NULL, _T("open"), m_chkChrome.GetCheck()?"whale":"iexplore", strFull, NULL, SW_SHOW);
	
	return 0;
}

CString Csucretary_kimDlg::GetPortalName(CString strInput)
{
	CString strBuf;
	CString strURL;
	strBuf = strInput;
		
	if (0 <= strBuf.Find(_PORTAL_NAME_NAVER_KO)) {
		strURL.Format("http://search.naver.com/search.naver?where=nexearch&query=");
	}
	else if (0 <= strBuf.Find(_PORTAL_NAME_NAVER_EN)) {
		strURL.Format("http://search.naver.com/search.naver?where=nexearch&query=");
	}
	else if (0 <= strBuf.Find(_PORTAL_NAME_GOOGLE_KO)) {
		strURL.Format("https://www.google.co.kr/search?q=");
	}
	else if (0 <= strBuf.Find(_PORTAL_NAME_GOOGLE_EN)) {
		strURL.Format("https://www.google.co.kr/search?q=");
	}
	else {// Default 구글	
		strURL.Format("https://www.google.co.kr/search?q=");
	}

	return strURL;
}

CString Csucretary_kimDlg::GetSearchString(CString strInput)
{
	CString strSearch;
	CString strBuf;
	strBuf = strInput;
	
	int indexEnd   = 0;
	int IndexStart = 0;
	
    indexEnd = strBuf.Find("검색");
	if (0 > indexEnd) {
		indexEnd = strBuf.Find(".");
	}

	if (0 <= (IndexStart = strBuf.Find(_PORTAL_NAME_NAVER_KO))) {
		strSearch = strBuf.Mid((IndexStart+strlen(_PORTAL_NAME_NAVER_KO)), (indexEnd-IndexStart)-(strlen(_PORTAL_NAME_NAVER_KO)));
	}
	else if (0 <= (IndexStart = strBuf.Find(_PORTAL_NAME_NAVER_EN)) ) {
		strSearch = strBuf.Mid((IndexStart+strlen(_PORTAL_NAME_NAVER_EN)), (indexEnd-IndexStart)-(strlen(_PORTAL_NAME_NAVER_EN)));
	}
	else if (0 <= (IndexStart = strBuf.Find(_PORTAL_NAME_GOOGLE_KO)) ) {
		strSearch = strBuf.Mid((IndexStart+strlen(_PORTAL_NAME_GOOGLE_KO)), (indexEnd-IndexStart)-(strlen(_PORTAL_NAME_GOOGLE_KO)));
	}
	else if (0 <= (IndexStart = strBuf.Find(_PORTAL_NAME_GOOGLE_EN)) ) {
		strSearch = strBuf.Mid((IndexStart+strlen(_PORTAL_NAME_GOOGLE_EN)), (indexEnd-IndexStart)-(strlen(_PORTAL_NAME_GOOGLE_EN)));
	}
	else {// 네이버/구글 문자열 생략 시	
		strSearch = strBuf.Mid(0, indexEnd);
	}

	
	return strSearch;
}

void Csucretary_kimDlg::OnEnSetfocusEditInput()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// Default 언어 한글
	HIMC himc = ImmGetContext(GetDlgItem(IDC_EDIT_INPUT)->GetSafeHwnd());
	ImmSetConversionStatus(himc, IME_CMODE_NATIVE, IME_SMODE_CONVERSATION);
	ImmReleaseContext(GetDlgItem(IDC_EDIT_INPUT)->GetSafeHwnd(), himc);
}

void Csucretary_kimDlg::CreateTrayIcon()
{
	ZeroMemory(&m_nid, sizeof(NOTIFYICONDATAEX));

	m_nid.hWnd = m_hWnd;		
	m_nid.hIcon = m_hIcon;
	m_nid.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE;
	m_nid.uID = IDI_ICON_TRAY;			

	m_nid.uCallbackMessage = WM_TRAY_EVENT;	

	::Shell_NotifyIcon(NIM_ADD,(PNOTIFYICONDATA)&m_nid);	
}

LRESULT Csucretary_kimDlg::OnTrayMessage(WPARAM wParam,LPARAM lParam)
{	
	if(LOWORD(lParam) == WM_LBUTTONDBLCLK) 
	{
		ShowWindow(SW_SHOW);
		ShowWindow(SW_RESTORE);
	}
	else if(LOWORD(lParam) == WM_RBUTTONUP)
	{
		CMenu menu, *pMenu;
		if (!menu.LoadMenu(IDR_MENU_TRAY))
			return 0;

		CPoint pt;
		pMenu = menu.GetSubMenu(0);
		GetCursorPos(&pt);

		// SetForegroundWindow()를 해줘야 다른곳에 마우스를 클릭하면 메뉴가 사라진다.
		SetForegroundWindow();

		UINT uMenuID = pMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, this);
		if(uMenuID == ID_FILES_RUN)
		{
			ShowWindow(SW_SHOW);
			ShowWindow(SW_RESTORE);
		}
		else if(uMenuID == ID_FILES_ADD)
		{
			CChildDataDlg dlg;
			dlg.DoModal();
		}
		else if (uMenuID == ID_FILES_EXIT)
		{
			OnOK();
		}

		::PostMessage(m_hWnd, WM_NULL, 0, 0);
	}

	//////////////////////////////////////////////////////////////////////////

	return 1;
}

BOOL Csucretary_kimDlg::PreTranslateMessage(MSG* pMsg)
{
	// ESC 및 엔터키를 막는다.
	if (pMsg->message == WM_KEYDOWN) {
		if ((pMsg->wParam ==  VK_ESCAPE)) {
			ShowWindow(SW_HIDE);
			return FALSE;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

LRESULT Csucretary_kimDlg::OnHotKey(WPARAM wParam, LPARAM lParam)
{
	if ((int)wParam == m_nHotKeyID) {
		ShowWindow(SW_SHOW);
		ShowWindow(SW_RESTORE);
		SetDlgItemText(IDC_EDIT_INPUT, "");
		GetDlgItem(IDC_EDIT_INPUT)->SetFocus();
	}
	else if ((int)wParam == m_nHotKeyID_FileDelete) {
		//MessageBox("휴지통을 거치지 않고 삭제 시 복구가 불가능 합니다.\n휴지통을 이용해 주시기바랍니다.", "경고", MB_ICONWARNING);
		keybd_event(VK_DELETE, 0, KEYEVENTF_EXTENDEDKEY, 0);
		keybd_event(VK_DELETE, 0, KEYEVENTF_KEYUP , 0 );
	}

	return 0;
}



void Csucretary_kimDlg::OnTimer(UINT_PTR nIDEvent)
{
	if (TIMER_KEY_DEBUG_START == nIDEvent)
	{
		ShellExecute(NULL, NULL, "C:\\Program Files (x86)\\NetMan\\PClient\\ThinPClient.exe", "-protectionoff puca", NULL, SW_SHOW);
	}

	CDialogEx::OnTimer(nIDEvent);
}
